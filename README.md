# Jest example
Example Node.js application with unit tests using the Jest test framework.

## Instructions
Run the following commands from a terminal:
```sh
# Download example:
git clone https://gitlab.com/ntnu-dcst2002/jest
cd jest
npm install # Install dependencies
npm start   # Run src/app.js
npm test    # Run tests in the tests-folder
```

Note that when creating new tests in the tests folder, the test filename has to
end with `Test.js`, for example `utilityTest.js`.
