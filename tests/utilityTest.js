// @flow

import { add } from '../src/utility.js';

describe('Utility tests', () => {
  it('add function return correct values', () => {
    expect(add(2, 2)).toBe(4);
    expect(add(-2, -2)).toBe(-4);
  });
});
